# A QUICK TASTE

Example in how to use [clap](https://github.com/clap-rs/clap) to build a CLI in Rust from their
[home page](https://clap.rs/).


## Build

```bash
cargo build --release
```

## Run

This is a very basic command that only prints a basic help.

```bash
$ ./target/release/a-quick-taste --help
myapp 1.0
Kevin K.
Does great things!

USAGE:
    a-quick-taste

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
```
